# Junior Frontend Test

## Obiettivo

Sviluppare un'applicazione web per la gestione di una lista utenti.

I dati sono disponibili tramite API REST al seguente URL: [https://gorest.co.in/public/v2/users](https://gorest.co.in/public/v2/users).



### Test React

Il progetto dovrà essere realizzato utilizzando lo starter kit: create-react-app.

Il progetto dovrà utilizzare `Typescript` e React Hooks.

Per il framework grafico è possibile scegliere tra:
- Chakra UI
- Material UI (V5)
- vanilla CSS



È preferibile l'utilizzo delle features CSS:
- Flexbox
- Grid
