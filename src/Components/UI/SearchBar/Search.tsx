import classes from "./Search.module.css"
import Button from '@mui/material/Button';

export default function Search() {



    return (
        <div className={classes["container"]}>
            <div className={classes["search"]}>
                
                    <input
                        type="text"
                        className={classes['my-input']}
                        placeholder=' &#xF002; Search '
                    />


                
            </div>
            <div className={classes["filter"]}>
                <Button variant="outlined"
                    size="small"
                    className={classes["my-button"]}
                    sx={{
                        border: "1px solid #d9d8dc",
                        color: "#7a8094",
                        borderRadius: "13px",
                        minWidth: "100px",
                        padding: "5px",
                        fontSize: "14px",
                        height: "32px",
                        marginLeft:"auto",
                    }}
                >
                    <i className="fa-solid fa-filter mr10"></i>Filters</Button>
                <Button variant="outlined"
                    size="small"
                    className={classes["my-button"]}
                    sx={{
                        border: "1px solid #d9d8dc",
                        color: "#7a8094",
                        borderRadius: "13px",
                        marginLeft: "5px",
                        fontSize: "16px",
                        height: "32px",
                    }}
                >
                    <i className="fa-solid fa-calendar-days"></i></Button>
                    <Button variant="outlined"
                    size="small"
                    className={classes["my-button-mobile"]}
                    sx={{
                        border: "1px solid #d9d8dc",
                        color: "#7a8094",
                        borderRadius: "13px",
                        marginLeft: "5px",
                        fontSize: "16px",
                        height: "32px",
                    }}
                >
                    <i className="fa-solid fa-sliders"></i></Button>

            </div>
        </div>


    )
}