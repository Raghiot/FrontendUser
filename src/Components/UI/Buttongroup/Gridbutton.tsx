import * as React from 'react';
import Box from '@mui/material/Box';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';



export default function ToggleButtonStatus() {
  const [alignment, setAlignment] = React.useState('All');  
  

  const handleChange = (
    event: React.MouseEvent<HTMLElement>,
    newAlignment: string,
  ) => {
    setAlignment(newAlignment);
    
    
    
    
  };
 

  const children = [
    <ToggleButton value="grid" key="grid"  sx={{
      borderRadius: "8px !important",
      padding:"15px",
      border: "none",
      
      '&:focus': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:active': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:hover': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&::selection': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      
      
      
    }}>
      <i className="fa-solid fa-bars"></i>
    </ToggleButton>,
    <ToggleButton value="list" key="list" sx={{
      borderRadius: "8px !important",
      padding:"15px",
      border: "none",
      
      '&:focus': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:active': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:hover': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&::selection': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", }
    }}>
      <i className="fa-solid fa-table-cells"></i>
    </ToggleButton>,
    
  ];

  const control = {
    value: alignment,
    onChange: handleChange,
    exclusive: true,
  };

  return (

    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        marginLeft: '10px',
        marginTop: "10px",




        // TODO Replace with Stack
        '& > :not(style) + :not(style)': { mt: 2 },
      }}
    >
      <ToggleButtonGroup size="small" {...control} sx={{
        backgroundColor: "#F7F9FC !important",
        width: "min-content",
        padding:"2px 15px",
        borderRadius:"15px"
        // display:"inline-block"

      }}>
        {children}
      </ToggleButtonGroup>

    </Box>
  );
}