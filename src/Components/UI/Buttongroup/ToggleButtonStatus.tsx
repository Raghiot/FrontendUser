import * as React from 'react';
import Box from '@mui/material/Box';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';

import { UserContext } from '../../context';
type User = {
  id?: number;
  name?: string;
  email?: string;
  gender?: string;
  status?: string;
  page?: number;
}

type Props = {
  user: User[];
  

  
  
}

export default function ToggleButtonStatus(props: Props) {
  const [alignment, setAlignment] = React.useState('All');
  const {user,setUser, userActive, allUser, userInactive} = React.useContext(UserContext)
  
  
  
  
  

  const handleChange = (
    event: React.MouseEvent<HTMLElement>,
    newAlignment: string,
  ) => {
    setAlignment(newAlignment);
    
    

    if(newAlignment.toLowerCase()=="active"){
      userActive(newAlignment)
      
    }else if(newAlignment.toLowerCase()=="inactive"){
      
      userInactive(newAlignment)
    }else{
      
      allUser(newAlignment)}
    
    
    
  };
 

  const children = [
    <ToggleButton value="All" key="All"  sx={{
      borderRadius: "8px !important",
      marginRight: "10px",
      border: "none",
      padding:"10px",
      '&:focus': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:active': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:hover': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&::selection': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      
      
      
    }}>
      All
    </ToggleButton>,
    <ToggleButton value="Active" key="Active" sx={{
      borderRadius: "8px !important",
      marginRight: "10px",
      border: "none",
      padding:"10px",
      '&:focus': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:active': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:hover': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&::selection': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", }
    }}>
      Active
    </ToggleButton>,
    <ToggleButton value="Inactive" key="Offline" sx={{
      borderRadius: "8px !important",
      marginRight: "10px",
      border: "none",
      padding:"10px",
      '&:focus': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:active': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:hover': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&::selection': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", }
    }}>
      Offline
    </ToggleButton>,
    <ToggleButton value="Archived" key="Archived" sx={{
      borderRadius: "8px !important",
      marginRight: "10px",
      border: "none",
      padding:"10px",
      '&:focus': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:active': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&:hover': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", },
      '&::selection': { backgroundColor: "white !important",boxShadow: "0px 1px 4px rgba(0, 0, 0, 0.05), 0px 6px 24px rgba(0, 0, 0, 0.04), inset 0px 1px 1px rgba(0, 0, 0, 0.04)", }

    }}>
      Archived
    </ToggleButton>,
  ];

  const control = {
    value: alignment,
    onChange: handleChange,
    exclusive: true,
  };

  return (

    <Box
    className='d600none'
      sx={{
        display: 'flex',
        flexDirection: 'column',
        marginLeft: '10px',
        marginTop: "10px",




        // TODO Replace with Stack
        '& > :not(style) + :not(style)': { mt: 2 },
      }}
    >
      <ToggleButtonGroup size="small" {...control} sx={{
        backgroundColor: "#F7F9FC !important",
        width: "min-content",
        padding:"2px 15px",
        borderRadius:"15px"
        // display:"inline-block"

      }}>
        {children}
      </ToggleButtonGroup>

    </Box>
  );
}