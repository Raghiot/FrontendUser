import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Pagination from '@mui/material/Pagination';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Stack from '@mui/material/Stack';
import * as React from 'react';
import { useEffect } from "react";
import { UserContext } from '../../context';
import Gridbutton from '../Buttongroup/Gridbutton';
import Card from "../Card/Card";
import classes from "./Onlineuser.module.css";




type User = {
    id?: number;
    name?: string;
    email?: string;
    gender?: string;
    status?: string;
    
}

type Props = {
    user: User[];



}

export default function Onlineusers(props: Props) {
    const { user, setUser, userLength } = React.useContext<any>(UserContext)
    const [page, setPage] = React.useState<any>(1)
    //const [age, setAge] = React.useState('');


    const handleChange = (event: SelectChangeEvent) => {
        setPage(event.target.value);


    };


    useEffect(() => {

        
        setUser((u:any) => u.sort(() => Math.random() - Math.random()).slice(0, userLength))

    }, [page, setUser, userLength])
    // const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
    //     '& .MuiBadge-badge': {
    //         right: -3,
    //         top: 13,
    //         border: `2px solid ${theme.palette.background.paper}`,
    //         padding: '0 4px',
    //     },
    // }));




    return (
        <>
            <div className="w100">
                <div className="w100children jcb d-flex">
                    <div className='d-flex'>
                    <h2>User <span className={classes.myBadge}> {props.user.length} </span></h2>
                    
                    
                    </div>
                    <Gridbutton></Gridbutton>
                </div>
            </div>
            <div className="w3">
                {user?.slice(0, 3).map((el: any) => {

                    return <Card key={el.name} {...el}>{el.name}</Card>

                })}
            </div>
            <div className={classes["grid-container"] + " jcc"}>


                {props.user?.slice(0, 12).map((el: any) => {

                    return <Card key={el.name} {...{ ...el, page }}>{el.name}</Card>

                })

                }

            </div>
            {(props.user.length >= 12) ? <div className="d-flex jcb d600none">
                <Stack spacing={2} >

                    <Pagination onChange={(event: React.ChangeEvent<unknown>, page: number) => { setPage(page) }} count={10} variant="outlined" shape="rounded" page={page} sx={{
                        '& MuiPaginationItem-root': { padding: "0px !important", margin: "0px !important" }
                    }} />
                </Stack>
                <FormControl size="small" >
                    <InputLabel id="demo-simple-select-label"></InputLabel>

                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={page}
                        label=""
                        onChange={handleChange}
                    >

                        {/* <MenuItem value={page}>{page}</MenuItem> */}

                        <MenuItem value={1} key={1}>1</MenuItem>
                        <MenuItem value={2} key={2}>2</MenuItem>
                        <MenuItem value={3} key={3}>3</MenuItem>
                        <MenuItem value={4} key={4}>4</MenuItem>
                        <MenuItem value={5} key={5}>5</MenuItem>
                        <MenuItem value={6} key={6}>6</MenuItem>
                        <MenuItem value={7} key={7}>7</MenuItem>
                        <MenuItem value={8} key={8}>8</MenuItem>
                        <MenuItem value={9} key={9}>9</MenuItem>
                        <MenuItem value={10} key={10}>10</MenuItem>


                    </Select>
                </FormControl>

            </div> : ""}

        </>




    )




}