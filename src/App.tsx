
import './App.css';
import { UserProvider } from './Components/context';
import Home from './Components/view/Home/Home';

// Ciao ragazzi, molto probabilmente typescript e Mui non sono utilizzati nel modo migliori,
// ho cercato di capire il funzionamento man mano che sviluppavo i componenti
// non avendo mai avuto a che fare con queste due tecnologie.
// Non sono stati eseguiti i test perche non sapevo che test fare in quanto gli unici che ho fatto sono
// in tdd con esercizi e mai su frontend veri e propri.
// il container swiper non e come da Figma.

//Attendo un vostro feedback, qualsiasi esso sia.
//Grazie

function App() {
  
  return (
    <div className="App">
      <UserProvider>
      <Home />
      </UserProvider>

    </div>
  );
}

export default App;
