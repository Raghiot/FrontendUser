import {useState, useContext , createContext, useEffect} from 'react'
import * as React from 'react';
type User = {
    id?: number;
    name?: string;
    email?: string;
    gender?: string;
    status?: string;
    page?: number;
}

type Props = {   
    
    
    children: JSX.Element;
    
    
}


export const UserContext = React.createContext<any>([])

export function UserProvider(props: Props){
    

    

    const [user, setUser] = React.useState([])
    const [userFilter, setUserFilter]=React.useState("")
    const [userOnline,setUserOnline]=React.useState("")
    

    const userActive = (props: Props)=>{
        setUserFilter('active')
        

        
        

    } 
    const userInactive = (props: Props)=>{
        setUserFilter('inactive')
        

        
        

    }
    const allUser = (props: Props)=>{
        setUserFilter('all')
        

        

    }
    
    
    useEffect(() => {
        fetch('https://gorest.co.in/public/v2/users')
            .then((response) => response.json())
            .then((data) => {
                setUserOnline(data.filter(((el: User) => { return el.status == 'active'.toLowerCase() })))
                console.log(`userfilter ${userFilter}`);
                // setUser(data)
                console.log("sono nel provider", data);
                if(userFilter=="active"){
                    
                    
                    setUser(data.filter((el: User)=>{return el.status=='active'.toLowerCase()}))
                    

                }else if(userFilter=="inactive"){
                    setUser(data.filter((el: User)=>{return el.status=='inactive'.toLowerCase()}))
                    

                }else{
                    setUser(data)
                    
                }
                


                


            })

    }, [userFilter])


    

    
    return (
        <UserContext.Provider value={{user,setUser,userActive,allUser,userInactive,userOnline}}>
            {props.children}
        </UserContext.Provider>

    )


}