import * as React from 'react';
// import Swiper core and required modules
import { Pagination } from 'swiper';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import { Swiper, SwiperSlide } from 'swiper/react';
import { UserContext } from "../../context";
import BottomBar from "../../UI/BottomBar/BottomBar";
import ToggleButtonStatus from "../../UI/Buttongroup/ToggleButtonStatus";
import Card from "../../UI/Card/Card";
import Onlineusers from "../../UI/Onlineusers/Onlineuser";
import Search from "../../UI/SearchBar/Search";
import classes from "../Home/Home.module.css";




// type User = {
//     id?: number;
//     name?: string;
//     email?: string;
//     gender?: string;
//     status?: string;
//     page?: number;
// }

// type Props = {
//     user: User[];
//     myFunction: any;



// }



export default function Home(){
    const { user, userOnline, setUser } = React.useContext(UserContext)

    // const [swiper, setSwiper] = React.useState(0)




    return (

        <div className="w100">
            <Search />
            <div className="w100">
                <div className="row jcb ml10">
                    <div className="">
                        <h1>Users</h1>
                    </div>
                    <div className="">
                        <button className={classes['add-button'] + " mr10"}>+ Add new</button>
                        <button className={classes['more-button'] + " mr10"}>...</button>
                    </div>

                </div>

            </div>
            <div className="wAuto">
                <ToggleButtonStatus user={user} />
            </div>
            <div className="w100">
                <div className="w100children d-flex jcb">

                    <div className="">
                        <h3>Users Online</h3>
                    </div>
                    <div className="d-flex aic">
                        <div className={classes.myDot}>

                        </div>
                        <div className={classes.myDot}>

                        </div>
                        <div className={classes.myDot}>

                        </div>
                        <div className={classes.myDot}>

                        </div>
                        <button className={classes.myButtonL}> {"<"} </button>
                        <button className={classes.myButtonR}> {">"} </button>
                    </div>


                </div>
            </div>
            <div className="w100">
                <div className="w100children d-flex aic">
                    <div className={classes.newChat}>
                        <div className={classes.newChatAdd}><p className={classes.plus}>+</p></div>
                        <p>New Chat</p>
                    </div>
                    {userOnline && <Swiper
                        slidesPerView={3}
                        spaceBetween={30}
                        pagination={{
                            clickable: true,
                        }}
                        modules={[Pagination]}
                        className="mySwiper d600none"
                        

                    >
                        {userOnline.map((el: any) => {
                            return <SwiperSlide key={el.name}><Card key={el.name} {...el}>{el.name}</Card></SwiperSlide>

                        })}


                    </Swiper>}
                    {userOnline && <Swiper
                        slidesPerView={1}
                        spaceBetween={30}
                        pagination={{
                            clickable: true,
                        }}
                        modules={[Pagination]}
                        className="mySwiper dnone"
                        

                    >
                        {userOnline.map((el: any) => {
                            return <SwiperSlide key={el.name}><Card key={el.name} {...el}>{el.name}</Card></SwiperSlide>

                        })}


                    </Swiper>}

                    

                </div>
            </div>








            <div className="w100 d-flex jcc">
                <div className="w100children">
                

                    <Onlineusers user={user}></Onlineusers>
                </div>
            </div>


            <div className="w100children">

                <button className="loadMoreButton dnone" onClick={() => setUser([...user, user])}>
                    <p><i className="fa-solid fa-arrow-down"></i></p>
                    <p>Load more</p>
                </button>
            </div>
            
            <BottomBar />


        </div>




    )
}


