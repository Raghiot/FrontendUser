
import classes from "./../Card/Card.module.css"

type User = {
  id?: number;
  name?: string;
  email?: string;
  gender?: string;
  status?: string;
  page?: number;
}





export default function Card(props: User) {

    
    


    return (
        <div className={classes["card-container"]}>

            <div className={classes["card"]}>
                <div className={classes['user-container']}>
                    <img src={`https://randomuser.me/api/portraits/men/${Math.floor(Math.random()*10)}.jpg`} alt="Blabla"/>
                    {props.status === "active" ? <div className={classes['status-user-online']}>
                    </div> : <div className={classes['status-user']}>
                    </div>


                    }

                </div>
                <div className={classes['icon-container']}>
                    <div className={classes['icon-bg']}><i className="fa-solid fa-message"></i></div>
                    <div className={classes['icon-bg']}>...</div>

                </div>

            </div>
            <div className="ml10">
                <h4>{props.name}</h4>
                <p>Project Designer</p>
            </div>
            <div className="ml10">
                
            </div>
        </div>

    )




}